package thesis.genetic.metavolution.evaluation;

import org.jenetics.Phenotype;
import thesis.configuration.genetic.Configuration;
import thesis.genetic.evolvers.Evolver;
import thesis.genetic.models.NeuralGene;

/**
 * Created by Alex on 03/08/2016.
 */
public class MetaEvaluator {

    private static final double ALPHA = 0.001;
    private static final Double BETA = 0.999;
    private final Evolver evolver;

    public MetaEvaluator(Evolver evolver) {
        this.evolver = evolver;
    }

    public Double eval(Configuration configuration) {
        Phenotype<NeuralGene, Double> bestPhenotype = evolver.evolve(configuration);

        return extractMetaFitnessFromPhenotype(bestPhenotype);
    }

    private Double extractMetaFitnessFromPhenotype(Phenotype<NeuralGene, Double> bestPhenotype) {
        return (-1d * bestPhenotype.getGeneration() * ALPHA) + bestPhenotype.getFitness() * BETA;
    }

    private double funct(double x) {
        return (1 - Math.exp(-1 / x));
    }


}
